local set = vim.keymap.set

set("n", "<leader>,", "<cmd>w<cr>")
set("n", "<leader>q", "<cmd>q<cr>")
set("n", "<leader>t", "<cmd>tabnew %<cr>")
set("n", "<leader>s", "<cmd>vsplit<cr>")
set("n", "<leader>n", "<cmd>noh<cr>")
set("n", "<leader>e", ":e ")

set("n", "<leader>p", '<cmd>set paste<CR>"*p:set nopaste<CR>')
set("n", "<leader>P", '<cmd>set paste<CR>"+p:set nopaste<CR>')

set("v", "<leader>y", '"*y')
set("v", "<leader>Y", '"+y')

set("v", "p", "yP")

set("n", "-", function()
	if vim.w.netrw_rexdir then
		vim.cmd.Rexplore()
	else
		vim.cmd.Explore()
	end
end)

-- Commentary
set("v", "<C-c>", "<cmd>normal gcc<cr>")
set(
	"n",
	"<C-c>",
	'":<C-u>normal " . v:count1 . "gcc<cr>"',
	{ expr = true, silent = true }
)

set("n", "<A-h>", "<cmd>m .+1<cr>==", { desc = "Move Down" })
set("n", "<A-t>", "<cmd>m .-2<cr>==", { desc = "Move Up" })
set("i", "<A-h>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move Down" })
set("i", "<A-t>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move Up" })
set("v", "<A-h>", ":m '>+1<cr>gv=gv", { desc = "Move Down" })
set("v", "<A-t>", ":m '<-2<cr>gv=gv", { desc = "Move Up" })

set("n", "mm", "%")

for n = 1, 9 do
	set("n", "<m-" .. n .. ">", n .. "gt")
end

set({ "n", "x", "o" }, "h", "j")
set({ "n", "x", "o" }, "t", "k")
set({ "n", "x", "o" }, "j", "h")
set({ "n", "x", "o" }, "k", "l")

set({ "n", "x", "o" }, "l", "t")
set({ "n", "x", "o" }, "gs", "0")
set({ "n", "x", "o" }, "gl", "$")

set("n", "<C-h>", "<C-d>")
set("n", "<C-t>", "<C-u>")

set("n", "<C-w>s", "<c-w>v")

set("n", "<C-w>h", "<c-w>j")
set("n", "<C-w>t", "<c-w>k")
set("n", "<C-w>d", "<c-w>h")
set("n", "<C-w>n", "<c-w>l")

set("n", "<C-w>H", "<c-w>J")
set("n", "<C-w>T", "<c-w>K")
set("n", "<C-w>D", "<c-w>H")
set("n", "<C-w>N", "<c-w>L")
