local o = vim.opt
local g = vim.g

g.netrw_banner = 0
g.netrw_list_hide = string.gsub("^../$,^./$", "%.", "\\%.")

o.colorcolumn = "80"
o.list = true
o.listchars = {
	trail = "•",
	space = "·",
	nbsp = "␣",
	tab = "→ ",
	eol = "¶",
	extends = "»",
	precedes = "«",
}

o.gdefault = true

o.hidden = false

o.autowrite = true

o.shortmess:append("W")

o.splitright = true
o.switchbuf = { "useopen", "usetab", "newtab" }

-- indent with tabs
o.tabstop = 4
o.softtabstop = 4
o.shiftwidth = 4
o.expandtab = false

g.rust_recommended_style = 0
g.rustfmt_autosave = 1
g.rustfmt_options = "--config hard_tabs=true"
