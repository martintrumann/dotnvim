local function augroup(name)
	return vim.api.nvim_create_augroup(name, { clear = true })
end

-- close some filetypes with <q>
vim.api.nvim_create_autocmd("FileType", {
	group = augroup("close_with_q"),
	pattern = {
		"PlenaryTestPopup",
		"help",
		"lspinfo",
		"notify",
		"qf",
		"spectre_panel",
		"startuptime",
		"tsplayground",
		"neotest-output",
		"checkhealth",
		"neotest-summary",
		"neotest-output-panel",
	},
	callback = function(event)
		vim.bo[event.buf].buflisted = false
		vim.keymap.set(
			"n",
			"q",
			"<cmd>close<cr>",
			{ buffer = event.buf, silent = true }
		)
	end,
})

-- overwrite t
vim.api.nvim_create_autocmd("FileType", {
	group = augroup("netrw"),
	pattern = { "netrw" },
	callback = function(ev)
		vim.keymap.set("n", "t", "k", { buffer = ev.buf })
	end,
})

-- Git commit tabstop to 4
vim.api.nvim_create_autocmd("FileType", {
	group = augroup("git_ident"),
	pattern = { "gitcommit" },
	callback = function()
		vim.opt_local.tabstop = 4
	end,
})
