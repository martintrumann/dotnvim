local ns = vim.api.nvim_create_namespace("format")

local err_handlers = {}
-- print stylua errors to diagnostics
err_handlers.stylua = function(buf, msg)
	local _, _, msg, extra = string.find(
		msg,
		": (.+)additional information: ([^\n]+)"
	)

	if msg == nil then
		vim.print("uhhh IDK: " .. msg)
		return
	end

	local s, _, line, col, e_l, e_c = string.find(
		msg,
		"%(starting from line (%d+), character (%d+) and ending on line (%d+), character (%d+)%)"
	)
	msg = string.sub(msg, 1, s - 1)
	_, _, unexpected = string.find(msg, "unexpected token `([^`]*)`")
	if unexpected then
		if unexpected == "" then
			line, col, e_l, e_c = line - 1, 0, line - 1, 0
			msg = extra .. ", found EOF"
		else
			msg = extra .. ', found "' .. unexpected .. '"'
		end
	else
		msg = msg .. ":" .. extra
	end

	vim.diagnostic.show(ns, buf, {
		{
			bufnr = buf,
			severity = vim.diagnostic.severity.ERROR,
			lnum = tonumber(line) - 1,
			col = tonumber(col) - 1,
			end_lnum = tonumber(e_l) - 1,
			end_col = tonumber(e_c) - 1,
			message = msg,
		},
	})
end

local function err(buf, err)
	vim.diagnostic.reset(ns, buf)
	if not err then
		return
	end
	local _, _, formatter = string.find(err, "'([%w$-_]+)'")
	local msg = ""
	if not err_handlers[formatter] then
		msg = err
	else
		msg = err_handlers[formatter](buf, err)
	end
end

return {
	{
		"stevearc/conform.nvim",
		lazy = false,
		-- ft = { "lua" },
		cmd = "ConformInfo",
		opts = {
			notify_on_error = false,
			format_on_save = function(buf)
				return {
					timeout_ms = 500,
					lsp_fallback = true,
				}, function(e)
					err(buf, e)
				end
			end,
			formatters_by_ft = {
				lua = { "stylua" },
			},
			formatters = {
				injected = {
					options = { ignore_errors = true },
				},
			},
		},
	},
}
