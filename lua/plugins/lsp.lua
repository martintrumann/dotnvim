local servers = {
	rust_analyzer = {
		settings = {
			["rust-analyzer"] = {
				check = { features = "all", command = "clippy" },
				cargo = {
					allFeatures = true,
				},
				procMacro = { enable = true },
				checkOnSave = { allTargets = true },
				diagnostics = {
					disabled = { "inactive-code" },
				},
				interpret = { tests = true },
			},
		},
	},
	t,
}

return {
	"neovim/nvim-lspconfig",
	lazy = false,
	keys = {
		{ "<leader>g", "<cmd>lua vim.diagnostic.open_float()<CR>" },
		{ "<leader>c", "<cmd>lua vim.diagnostic.goto_next()<CR>" },
		{ "<leader>h", "<cmd>lua vim.lsp.buf.hover()<CR>" },
		{ "<leader>r", "<cmd>lua vim.lsp.buf.rename()<CR>" },
		{
			"<leader>a",
			"<cmd>lua vim.lsp.buf.code_action()<CR>",
			mode = { "v", "n" },
		},
	},
	config = function()
		local l = require("lspconfig")
		for k, v in pairs(servers) do
			local capabilities = vim.tbl_deep_extend(
				"force",
				{},
				vim.lsp.protocol.make_client_capabilities(),
				require("cmp_nvim_lsp").default_capabilities()
			)
			l[k].setup(v)
		end
	end,
}
